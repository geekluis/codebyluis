var appMana = angular.module('webPersonal', []);

appMana.controller('controllerPortafolio',['$scope', function($scope){
  $scope.caja = function(data){
    $("#box"+data).animate({
      opacity: 0.9,
    })
  };
  $scope.ocultaCaja = function(data){
    $("#box"+data).animate({
      opacity: 0,
    })
  }
}]);